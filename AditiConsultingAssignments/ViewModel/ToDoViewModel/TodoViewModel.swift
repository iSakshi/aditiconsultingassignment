//
//  TodoViewModel.swift
//  AditiConsultingAssignments
//
//  Created by Sakshi Bala on 24/06/20.
//  Copyright © 2020 Sakshi Bala. All rights reserved.
//

import UIKit
import CoreData

class TodoViewModel: NSObject {
    var reloadTableView = { () -> () in }
    
    //Data Binding
    var itemsArray : [Items] = [] {
        didSet {
            reloadTableView()
        }
    }

    
    func fetchResults(filter: String) {
        var savedItemsArray = [Items]()
        let results = findDataFromCoreData(filter: filter)
        for data in results {
            savedItemsArray.append(Items.init(model: data))
        }
        itemsArray = savedItemsArray
        sortArray()
    }

    func findDataFromCoreData(filter: String = "", id: Int32 = 0) -> [NSManagedObject] {
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: EntityNames.ItemEntity)
        if !filter.isEmpty {
            request.predicate = NSPredicate(format: "(title CONTAINS[c] %@) OR (descriptionText CONTAINS[c] %@)", filter,filter)
        }
        if id != 0 {
            request.predicate = NSPredicate(format: "itemID == \(id)")
        }
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            guard let results = result as? [NSManagedObject] else { return [] }
            return results
        } catch {
            return []
        }
    }
    
    func addNewItem(data: NSManagedObject) {
        itemsArray.insert(Items.init(model: data), at: 0)
        sortArray()
    }
    func updateObjectInItems(index: Int) {
        
        let context = appDelegate.persistentContainer.viewContext
        let model = itemsArray[index]
        model.isChecked = !model.isChecked
        
        let results = findDataFromCoreData(id: Int32(model.itemID))
        guard let itemObject = results.first else { return }
        itemObject.setValue(model.isChecked, forKey: KeyTitles.isChecked)
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }

        itemsArray.remove(at: index)
        itemsArray.insert(model, at: index)
        sortArray()
    }

    func sortArray() {
        let partition = itemsArray.partition(by: { $0.isChecked })
        itemsArray = Array(itemsArray[..<partition]) + Array(itemsArray[partition...])
    }
}
