//
//  AddItemViewModel.swift
//  AditiConsultingAssignments
//
//  Created by Sakshi Bala on 24/06/20.
//  Copyright © 2020 Sakshi Bala. All rights reserved.
//

import UIKit
import CoreData

class AddItemViewModel: NSObject {
    var ItemSavedSuccessfully : ((NSManagedObject) ->())?
    var ItemSaveFailed = { () -> () in }

    
    func saveData(title: String, descriptionText: String, itemID: Int) {
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: EntityNames.ItemEntity, in: context)
        let newUser = NSManagedObject(entity: entity!, insertInto: context)
        newUser.setValue(title, forKey: KeyTitles.title)
        newUser.setValue(descriptionText, forKey: KeyTitles.descriptionText)
        newUser.setValue(itemID, forKey: KeyTitles.itemID)
        newUser.setValue(false, forKey: KeyTitles.isChecked)
        do {
            try context.save()
            ItemSavedSuccessfully? (newUser)
        } catch {
            print("Failed saving")
            ItemSaveFailed()
        }
        
    }
    
}
