//
//  Item.swift
//  AditiConsultingAssignments
//
//  Created by Sakshi Bala on 24/06/20.
//  Copyright © 2020 Sakshi Bala. All rights reserved.
//

import UIKit
import CoreData
class Items {
   
    let title: String
    let descriptionText: String
    var isChecked: Bool
    var itemID: Int

    init(model: NSManagedObject) {
        self.title = model.value(forKey: KeyTitles.title) as? String ?? ""
        self.descriptionText = model.value(forKey: KeyTitles.descriptionText) as? String ?? ""
        self.isChecked = model.value(forKey: KeyTitles.isChecked) as? Bool ?? false
        self.itemID = model.value(forKey: KeyTitles.itemID) as? Int ?? 0
    }
}
