//
//  Constants.swift
//  AditiConsultingAssignments
//
//  Created by Sakshi Bala on 24/06/20.
//  Copyright © 2020 Sakshi Bala. All rights reserved.
//

import UIKit

let appDelegate = UIApplication.shared.delegate as! AppDelegate

struct Constants {
    public static let homeNavigationTitle = "To Do"
    public static let noResultsFound = "No Results Found"
    public static let searchBarPlaceholder = "Search here..."
}

struct NibNames {
    public static let ToDoTableViewCell = "ToDoTableViewCell"
}

struct ClassNames {
    public static let ToDoTableViewCell = "ToDoTableViewCell"
}

struct EntityNames {
    public static let ItemEntity = "ItemEntity"
}

struct KeyTitles {
    public static let title = "title"
    public static let descriptionText = "descriptionText"
    public static let isChecked = "isChecked"
    public static let itemID = "itemID"
}


struct StoryBoardIDS {
    public static let AddItemViewController = "AddItemViewController"
    public static let descriptionText = "descriptionText"
    public static let isChecked = "isChecked"

}

public extension UIViewController {
    @IBAction func unwindToViewController (_ segue: UIStoryboardSegue){}
}
