//
//  AddItemViewController.swift
//  AditiConsultingAssignments
//
//  Created by Sakshi Bala on 24/06/20.
//  Copyright © 2020 Sakshi Bala. All rights reserved.
//

import UIKit
import CoreData

class AddItemViewController: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    let addItemViewModel = AddItemViewModel()
    var savedItem : ((NSManagedObject) ->())?
    var savedItemsCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
    }
    
    //View Model Binding
    func setupViewModelBinding() {
        addItemViewModel.ItemSavedSuccessfully = { model in
            self.savedItem? (model)
            self.navigationController?.popViewController(animated: true)
        }
        addItemViewModel.ItemSaveFailed = {
            
        }
    }
    
    func setupView() {
        self.descriptionTextView.addDoneButton(title: "Done", target: self, selector: #selector(tapTextViewDone(sender:)))
        setupViewModelBinding()
    }

    
    //IBAction
    @objc func tapTextViewDone(sender: Any) {
        self.view.endEditing(true)
    }

    @IBAction func doneAction(_ sender: UIButton) {
        guard let title = titleTextField.text, !title.isEmpty, let descriptionText = descriptionTextView.text, !descriptionText.isEmpty  else { return }
        addItemViewModel.saveData(title: title, descriptionText: descriptionText, itemID: (savedItemsCount+1))
    }
    
}


extension AddItemViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        descriptionTextView.becomeFirstResponder()
        return true
    }
}
