//
//  SearchViewController+ToDoTableViewController.swift
//  AditiConsultingAssignments
//
//  Created by Sakshi Bala on 24/06/20.
//  Copyright © 2020 Sakshi Bala. All rights reserved.
//

import Foundation
import UIKit

extension ToDoTableViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //To limit activity, reload half a second after last key press.
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(getResults), object: nil)
        self.perform(#selector(getResults), with: nil, afterDelay: 0.5)
    }
    
    @objc func getResults() {
        todoViewModel.fetchResults(filter: searchBar.text ?? "")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}



