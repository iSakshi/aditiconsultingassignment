//
//  ToDoTableViewController.swift
//  AditiConsultingAssignments
//
//  Created by Sakshi Bala on 24/06/20.
//  Copyright © 2020 Sakshi Bala. All rights reserved.
//

import UIKit

class ToDoTableViewController: UITableViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    
    let todoViewModel = TodoViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    func setupTableView() {
        self.tableView.register(UINib(nibName: NibNames.ToDoTableViewCell, bundle: nil), forCellReuseIdentifier: ClassNames.ToDoTableViewCell)
        self.tableView.tableFooterView = UIView()
        self.tableView.allowsSelection = true
    }
    
    func setupSearchBar() {
        searchBar.placeholder = Constants.searchBarPlaceholder
        searchBar.delegate = self
        searchBar.showsSearchResultsButton = false
        searchBar.autocorrectionType = .no
        searchBar.returnKeyType = .default
    }
    
    func setupViewModelBinding() {
        todoViewModel.fetchResults(filter: (searchBar.text ?? ""))
        todoViewModel.reloadTableView = {
            self.tableView.reloadData()
        }
    }
    
    func setupView() {
        setupTableView()
        setupSearchBar()
        setupViewModelBinding()
    }

    @IBAction func addItemAction(_ sender: UIButton) {
        guard let vc = self.storyboard?.instantiateViewController(identifier: StoryBoardIDS.AddItemViewController) as? AddItemViewController else { return }
        vc.savedItemsCount = todoViewModel.itemsArray.count
        self.navigationController?.pushViewController(vc, animated: true)
        vc.savedItem = { model in
            self.todoViewModel.addNewItem(data: model)
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todoViewModel.itemsArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let toDoTableViewCell = tableView.dequeueReusableCell(withIdentifier: ClassNames.ToDoTableViewCell, for: indexPath) as? ToDoTableViewCell else { return UITableViewCell() }
        let model = todoViewModel.itemsArray[indexPath.row]
        toDoTableViewCell.setUpView(model: model)
        return toDoTableViewCell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        todoViewModel.updateObjectInItems(index: indexPath.row)
    }
}



